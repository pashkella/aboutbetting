import Swiper from "swiper";

window.onload = function () {
    let anchorLinks = document.querySelectorAll('a[href^="#"]');

    for (let item of anchorLinks) {
        item.addEventListener("click", (event) => {
            let hashval = item.getAttribute("href");
            let target = document.querySelector(hashval);
            target.scrollIntoView({
                behavior: "smooth",
                block: "start",
            });
            history.pushState(null, null, hashval);
            event.preventDefault();
        });
    }

    // search
    const search = document.getElementById("search");
    const searchButton = document.getElementById("search-button");
    const searchInput = document.getElementById("search-input");

    searchButton.addEventListener("click", () => {
        search.classList.toggle("is-active");
        this.setTimeout(() => searchInput.focus(), 600);
    });

    // bonus carousel
    const swiperBonus = new Swiper(".swiper-bonus", {
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
            nextEl: ".bookmaker-bonus-nav__next",
            prevEl: ".bookmaker-bonus-nav__prev",
        },
        breakpoints: {
            300: {
                slidesPerView: 1,
            },
            640: {
                slidesPerView: 2,
            },
            850: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 3,
            },
        },
    });

    // cats menu
    const subCategoriesButton = document.querySelectorAll(".submenu-toggle");

    if (subCategoriesButton)
        subCategoriesButton.forEach((el) =>
            el.addEventListener("click", (event) => {
                event.target.closest(".sidebar-sport-cats__item").classList.toggle("is-active");
            })
        );

    // share
    const shareButton = document.querySelector(".share");

    if (shareButton)
        shareButton.addEventListener("click", () => {
            shareButton.classList.add("is-hidden");
        });

    // mobile menu
    const mobileMenuButton = document.querySelector(".hamburger");
    const mobileMenu = document.querySelector(".main-menu--header");
    const mobileSubMenu = document.querySelectorAll(".mobile-open-sub");

    mobileMenuButton.addEventListener("click", (event) => {
        mobileMenuButton.classList.toggle("is-active");
        mobileMenu.classList.toggle("is-active");
    });
    mobileSubMenu.forEach((el) =>
        el.addEventListener("click", (event) => {
            event.target.parentNode.classList.toggle("is-open");
        })
    );

    // mobile cats
    const mobileCatsButton = document.querySelector(".sidebar-cats-title");

    if (mobileCatsButton)
        mobileCatsButton.addEventListener("click", (event) => {
            mobileCatsButton.parentNode.classList.toggle("is-open");
        });

    // bookmaker detailed mobile nav
    const bookmakerNavButton = document.querySelector(".bookmaker-menu-button");

    if (bookmakerNavButton)
        bookmakerNavButton.addEventListener("click", (event) => {
            bookmakerNavButton.parentNode.classList.toggle("is-open");
        });
};
