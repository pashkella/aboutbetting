
var	gulp = require('gulp'),
  pug = require('gulp-pug'),
  notify = require('gulp-notify'),
  cleancss = require('gulp-clean-css'),
  autoprefixer = require('gulp-autoprefixer'),
  rename = require('gulp-rename'),
  sass = require('gulp-sass'),
  connect = require('gulp-connect'),
  livereload = require('gulp-livereload'),
  svgSprite = require('gulp-svg-sprite'),
  svgmin = require('gulp-svgmin'),
  cheerio = require('gulp-cheerio'),
  replace = require('gulp-replace'),
  babel = require('gulp-babel'),
  browserify = require('browserify'),
  babelify = require('babelify'),
  source = require('vinyl-source-stream'),
  buffer = require('vinyl-buffer'),
  uglify = require('gulp-uglify');


var site = 'public',
  inputCss = 'source/stylesheets/sass/*.{sass,scss}',
  outputCss = 'source/stylesheets/css',
  outputMinCss = 'public/assets/css',
  inputJs = 'source/javascripts/main.js',
  outputMinJs = 'public/assets/js',
  inputSass = 'source/stylesheets/sass/**/*.{sass,scss}',
  inputPug = 'source/views/pages/*.pug',
  inputSvgInline = 'source/images/svg-inline/*.svg',
  inputSvgBg = 'source/images/svg-bg/*.svg',
  outputSvg = 'public/assets/images/icons';

gulp.task('connect', function(done) {
  connect.server({
    root: site,
    livereload: true,
    port: 8081
  });
  done();
});

gulp.task('views', function buildHTML(done) {
  gulp.src(inputPug)
    .pipe(pug({
      pretty: true
    }))
    .on('error', notify.onError(function (error) {
      return 'An error occurred while compiling Pug.\nLook in the console for details.\n' + error;
    }))
    .pipe(gulp.dest(site));
  done();
});

gulp.task('html', function(done){
  gulp.src(site+'/*.html')
    .pipe(connect.reload());
  done();
});

gulp.task('styles', function(done) {
  gulp.src(inputCss)
    .pipe(sass({
      outputStyle: 'expanded'
    }))
    .on('error', function(err) {
          notify().write(err);
          this.emit('end');
      })
    .pipe(autoprefixer('last 99 version'))
    .pipe(gulp.dest(outputCss));
  done();
});

gulp.task('cssmin', function(done) {
  gulp.src(outputCss+'/*.css')
    .pipe(rename({suffix: '.min'}))
    .pipe(cleancss())
    .pipe(gulp.dest(outputMinCss))
    .pipe(connect.reload());
  done();
});

gulp.task('js', function(done) {
  browserify({entries: inputJs, debug: true})
    .transform("babelify", { presets: ['@babel/env'] })
    .bundle()
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(uglify())
    .on('error', notify.onError(function (error) {
      return 'Javascript minification error.\nLook in the console for details.\n' + error;
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(outputMinJs))
    .pipe(connect.reload());
  done();
});

gulp.task('svg-inline', function (done) {
  gulp.src(inputSvgInline)
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    .pipe(replace('&gt;', '>'))
    .pipe(svgSprite({
      mode: {
        symbol: {
          sprite: "../sprite-inline.svg",
          render: {
            scss: {
              dest:'../../../../../source/stylesheets/sass/base/sprites/_svg-inline.scss',
              template: 'source/stylesheets/sass/base/sprites/_template-inline.scss'
            }
          }
        }
      }
    }))
    .pipe(gulp.dest(outputSvg));
  done();
});

gulp.task('svg-bg', function (done) {
  gulp.src(inputSvgBg)
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    .pipe(cheerio({
            run: function ($) {
        $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
    .pipe(replace('&gt;', '>'))
    .pipe(svgSprite({
      shape: {
        spacing: {
          padding: 5
        }
      },
      mode: {
        css: {
          sprite: "../sprite-bg.svg",
          bust: false,
          render: {
            scss: {
              dest:'../../../../../source/stylesheets/sass/base/sprites/_svg-bg.scss',
              template: 'source/stylesheets/sass/base/sprites/_template-bg.scss'
            }
          }
        }
      },
      variables: {
        mapname: "icons"
      }
    }))
    .pipe(gulp.dest(outputSvg));
  done();
});

gulp.task('clean', function(done) {
  connect.serverClose();
  done();
});

gulp.task('build', gulp.series('views', 'html', 'styles', 'cssmin', 'js', 'svg-inline', 'svg-bg', function(done) {
  done();
}));

gulp.task('watch', function() {
  gulp.watch(inputJs, gulp.series('js'));
  gulp.watch(outputCss+'/*.css', gulp.series('cssmin'));
  gulp.watch(inputCss, gulp.series('styles'));
  gulp.watch(inputSass, gulp.series('styles'));
  gulp.watch(inputSvgInline, gulp.series('svg-inline'));
  gulp.watch(inputSvgBg, gulp.series('svg-bg'));
  gulp.watch('source/views/**/*.pug', gulp.series('views'));
  gulp.watch(site+'/*.html', gulp.series('html'));
});

gulp.task('dev', gulp.series('clean', 'build', gulp.parallel('connect', 'watch')));
